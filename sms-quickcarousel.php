<?php
/*
Plugin Name: SMS QuickCarousel
Plugin URI: http://www.roadsidemultimedia.com
Description: Continuos Slider
Author: Curtis Grant
PageLines: true
Version: 1.0.4
Section: true
Class Name: SMSQuickCarousel
Filter: gallery, dual-width
Loading: active
Text Domain: dms-sms
Bitbucket Plugin URI: https://bitbucket.org/roadsidemultimedia/sms-quickcarousel
Bitbucket Branch: master
*/

if( ! class_exists( 'PageLinesSectionFactory' ) )
  return;


class SMSQuickCarousel extends PageLinesSection {

	function section_styles(){
		//CSS
		wp_enqueue_style( 'slick', $this->base_url.'/slick/slick.css');
		wp_enqueue_style( 'slick-custom', $this->base_url.'/css/style.css');
		
		wp_enqueue_script( 'slick', $this->base_url.'/slick/slick.min.js', array( 'jquery' ), pl_get_cache_key(), true );
	}

	function section_opts(){


		$options = array();
		
		$options[] = array(
				'type'	=> 'multi',
				'key'	=> 'config', 
				'title'	=> 'Config',
				'col'	=> 1,
				'opts'	=> array(
					array(
						'key'			=> 'desktop_max',
						'type' 			=> 'count_select',
						'count_start'	=> 1,
						'count_number'	=> 6,
						'default'		=> '6',
						'label' 	=> __( 'Max View for Desktop', 'pagelines' ),
					),
					array(
						'key'			=> 'tablet_max',
						'type' 			=> 'count_select',
						'count_start'	=> 1,
						'count_number'	=> 6,
						'default'		=> '3',
						'label' 	=> __( 'Max View for Tablet', 'pagelines' ),
					),
					array(
						'key'			=> 'smartphone_max',
						'type' 			=> 'count_select',
						'count_start'	=> 1,
						'count_number'	=> 6,
						'default'		=> '1',
						'label' 	=> __( 'Max View for Smartphone', 'pagelines' ),
					),
					array(
						'key'			=> 'slide_speed',
						'type' 			=> 'select',
						'label' 	=> __( 'Slider Speed', 'pagelines' ),
						'opts'      => array(
    			        		'1000'   => array('name' => '1 Second'),
  				        		'2000'   => array('name' => '2 Seconds'),
              					'3000'   => array('name' => '3 Seconds'),            
             					'4000'   => array('name' => '4 Seconds'),
              					'5000'   => array('name' => '5 Seconds'),
              					'6000'   => array('name' => '6 Seconds'),
            				)
					),
					array(
						'key'			=> 'img_opacity',
						'type' 			=> 'select',
						'label' 	=> __( 'Image Opacity', 'pagelines' ),
						'opts'      => array(
    			        		's100'   => array('name' => '100 '),
  				        		's90'   => array('name' => '90'),
              					's80'   => array('name' => '80'),            
             					's70'   => array('name' => '70'),
              					's60'   => array('name' => '60'),
              					's50'   => array('name' => '50'),
              					's40'   => array('name' => '40'),            
             					's30'   => array('name' => '30'),
              					's20'   => array('name' => '20'),
              					's10'   => array('name' => '10'),
            				)
					),
					array(
						'key'			=> 'img_opacity_hover',
						'type' 			=> 'select',
						'label' 	=> __( 'Image Opacity on Hover', 'pagelines' ),
						'opts'      => array(
    			        		'h100'   => array('name' => '100'),
  				        		'h90'   => array('name' => '90'),
              					'h80'   => array('name' => '80'),            
             					'h70'   => array('name' => '70'),
              					'h60'   => array('name' => '60'),
              					'h50'   => array('name' => '50'),
              					'h40'   => array('name' => '40'),            
             					'h30'   => array('name' => '30'),
              					'h20'   => array('name' => '20'),
              					'h10'   => array('name' => '10'),
            				)
					),
				)
				
			);
		
		
		$options[] = array(
			'key'		=> 'array',
	    	'type'		=> 'accordion', 
			'col'		=> 2,
			'opts_cnt'	=> 6,
			'title'		=> __('Image Setup', 'pagelines'), 
			'post_type'	=> __('Image', 'pagelines'), 
			'opts'	=> array(
				array(
					'key'		=> 'image',
					'label' 	=> __( 'Carousel Image <span class="badge badge-mini badge-warning">REQUIRED</span>', 'pagelines' ),
					'type'		=> 'image_upload',
				),
				array(
					'key'	=> 'link',
					'label'	=> __( 'Image Link', 'pagelines' ),
					'type'	=> 'text',
				),
				array(
            		'type'      => 'select',
            		'key'     => 'href_overlay',
            		'label'     => 'Link Overlay Style',
            		'opts'      => array(
              			'NONE'   => array('name' => 'NONE'),
              			'icon-search-plus'   => array('name' => 'Image'),
              			'icon-link'  => array('name' => 'Link'),
            		)
          		),
          		array(
            		'type'      => 'select',
            		'key'     => 'href_target',
            		'label'     => 'Link Target',
            		'opts'      => array(
              			'_self'   => array('name' => 'Same Window'),
              			'_blank'  => array('name' => 'New Window'),
            		)
          		),
				array(
					'key'	=> 'href_class',
					'label'	=> __( 'Link Class', 'pagelines' ),
					'type'	=> 'text',
				),
				array(
					'key'	=> 'href_id',
					'label'	=> __( 'Link ID', 'pagelines' ),
					'type'	=> 'text',
				),

			)
	    );
	
		
		return $options;

	}

	function section_head() {
		$dmax = ($this->opt('desktop_max')) ? $this->opt('desktop_max') : '';
		$tmax = ($this->opt('tablet_max')) ? $this->opt('tablet_max') : '';
		$smax = ($this->opt('smartphone_max')) ? $this->opt('smartphone_max') : '';
		$speed = ($this->opt('slide_speed')) ? $this->opt('slide_speed') : '4000';
		?>
		<script>
			jQuery(function($) {
    			$(document).ready(function() {
        			$('.qcresponsive').slick({
        			    dots: false,
     			    	infinite: true,
			            speed: 300,
			            slidesToShow: <?php echo "$dmax"; ?>,
            			slidesToScroll: 1,
            			autoplay: true,
            			autoplaySpeed: <?php echo "$speed"; ?>,
            			responsive: [{
                			breakpoint: 1024,
                			settings: {
                    			slidesToShow: <?php echo "$tmax"; ?>,
                    			infinite: true
                			}
            			}, {
                			breakpoint: 480,
                			settings: {
                			slidesToShow: <?php echo "$smax"; ?>,
                		}
            			}]
        			});
    			});
			});
			</script>
			<?php
	}

	function get_content( $array ){
		
		$cols = ($this->opt('cols')) ? $this->opt('cols') : 2;
		$hpad = ($this->opt('hpad')) ? 'hpad-' . $this->opt('hpad') : '';
		$vpad = ($this->opt('vpad')) ? 'vpad-' . $this->opt('vpad') : '';
		
		$out = '';
		
		if( is_array( $array ) ){
			foreach( $array as $key => $item ){
				$image = pl_array_get( 'image', $item ); 
				$image_id = pl_array_get( 'image_attach_id', $item ); 
				$link = pl_array_get( 'link', $item );
				$hclass = pl_array_get( 'href_class', $item );
				$hid = pl_array_get( 'href_id', $item );
				$hrefoverlay = pl_array_get( 'href_overlay', $item );
				$htarget = pl_array_get( 'href_target', $item );
				$imgopacity = ($this->opt('img_opacity')) ? $this->opt('img_opacity') : '';
				$imgopacityhover = ($this->opt('img_opacity_hover')) ? $this->opt('img_opacity_hover') : '';
			
				if( $image ){
				
					$image_meta = wp_get_attachment_image_src( $image_id, 'aspect-thumb' );	
					$image_url = (isset($image_meta[0])) ? $image_meta[0] : $image;
					if ($hrefoverlay=="NONE") {
						$image_out = ( $link ) ? sprintf('<a href="%s" class="%s" id="%s" target="%s"><section class="qc qcimage"><img src="%s" class="%s %s" /></section></a>', $link, $hclass, $hid, $htarget, $image_url, $imgopacity, $imgopacityhover) : sprintf('<img src="%s" class="%s %s" />', $image_url, $imgopacity, $imgopacityhover);
					}
					else {
						$image_out = ( $link ) ? sprintf('<a href="%s" class="%s" id="%s" target="%s"><div class="qcoverlay qczoom"><i class="icon %s"></i></div><section class="qc qcimage"><img src="%s" class="%s %s" /></section></a>', $link, $hclass, $hid, $htarget, $hrefoverlay, $image_url, $imgopacity, $imgopacityhover) : sprintf('<img src="%s" class="%s %s" />', $image_url, $imgopacity, $imgopacityhover);
					}
					$out .= sprintf(
						'<div>%s</div>',
						$image_out
					);
				}

			}
		}
		
		
		return $out;
	}

   function section_template( ) { 
	
		$classes = array(); 
		
		$max = ($this->opt('max')) ? $this->opt('max') : 6;
		
		$format = $this->opt('format');
		$classes[] = 'format-'.$format;
		
		$array = $this->opt('array');
	
	?>
	
	<div class="slider qcresponsive">
	
		<?php

		$out = $this->get_content( $array ); 

		if( $out == '' ){
			$array = array(
				array( 'image'		=> pl_default_image() ),
				array( 'image'		=> pl_default_image() ),
				array( 'image'		=> pl_default_image() ),
				array( 'image'		=> pl_default_image() ),
				array( 'image'		=> pl_default_image() ),
				array( 'image'		=> pl_default_image() ),
				array( 'image'		=> pl_default_image() ),
			);

			$out = $this->get_content( $array ); 
		} 

		echo $out;

		?>
	</div>

			

<?php }


}